//json
const products = [
        {
                  "img": "a.jpg",
                  "title":"Napolact BIO 1,5%",
                  "txt": "Napolact BIO este laptele ecologic pur din inima Ardealului. Un lapte de cea mai bun&#259 calitate, colectat numai de la fermele atent selec&#355ionate, care &icirc&#351i p&#259streaz&#259 toate calit&#259&#355ile d&#259ruite de natur&#259.",
                  "pret":"Pre&#355: 11<sup>77</sup> lei"
                },
                {
                    "img": "b.jpg",
                    "title":"President unt semi-s&#259rat 200 g",
                    "txt": "Unt President ambalat &icircn folie de aluminiu care nu se rupe, pachetul de unt se deschide &#351i se &icircnchide foarte u&#351or, astfel ca datorit&#259 acestui ambalaj, se p&#259streaz&#259 foarte bine aroma untului. Acest unt, de o calitate superioar&#259 &#351i cu ambalaj foarte atractiv este pe gustul consumatorilor de unt.",
                    "pret":"Pre&#355: 11<sup>40</sup> lei"
                },
                  {
                    "img": "c.jpg",
                    "title":"Ca&#351caval Dalia",
                    "txt": "Ob&#355inut din lapte de vac&#259, pasteurizat prin procedeul de op&#259rire. Poate con&#355ine lactoz&#259.Produs pasteurizat. Poate con&#355ine urme de lapte. A se p&#259stra la temperatura de 2-6 grade Celsius. A se agita bine &icircnainte de consum.",
                    "pret":"Pre&#355: 19<sup>14</sup> lei"
                  },
                  {
                    "img": "d.jpg",
                    "title":"Chefir Degresat",
                    "txt": "Produs acidolactic fabricat din lapte de vac&#259 pe baz&#259 de granule de chefir. Metoda de producere-la rezervor", 
                    "pret":"Pre&#355: 9<sup>58</sup> lei"
                  },
                  {
                    "img": "e.jpg",
                    "title":"Covalact sm&acircnt&acircn&#259 25% gr&#259sime 300 g ",
                    "txt": "Sm&acircnt&acircn&#259 fermentat&#259 cu 25% gr&#259sime. A se p&#259stra la temperaturi cuprinse &icircntre 2-8 grade Celsius. A se consuma, de preferin&#355&#259, &icircnainte de data &icircnscrip&#355ionat&#259 pe ambalaj.",
                    "pret":"Pre&#355: 6<sup>79</sup> lei"
                  },
                  {
                      "img": "f.jpg",
                      "title":"Iaurt Zuzu",
                      "txt": "Iaurt natural 3% gr&#259sime. Consumat zilnic, iaurtul Zuzu reprezint&#259 o sursa de proteine ce te hr&#259ne&#351te &#351i &icirc&#355i d&#259 energia care te pune &icircn ac&#355iune &icircntreaga zi.",
                      "pret":"Pre&#355: 1<sup>04</sup> lei"
                    },
                    {
                      "img": "g.jpg",
                      "title":"&Icircnghe&#355ata Nirvana",
                      "txt": "Lapte reconstituit din lapte praf degresat, zah&#259r, sm&acircnt&acircn&#259, ciocolat&#259 pudr&#259 (8.5%), ulei vegetal de cocos, sirop de glucoz&#259, zer praf, mas&#259 de cacao, sirop de glucoz&#259-fructoz&#259, lapte praf degresat, cacao pudr&#259 cu con&#355inut redus de gr&#259sime, unt de cacao, stabilizatori (alg&#259 Euchema prelucrat&#259, caragenan, gum&#259 de carruba, gum&#259 de guar), emulgatori (mono &#351i digliceride ale acizilor gra&#351i, lecitine (con&#355ine soia)), lapte condensat &icircndulcit, lapte praf integral, unt, colorant (caramel cu sulfit de amoniu), amidon modificat, praf de g&#259lbenu&#351 de ou, arome, acidifiant (acid citric), sare.",
                      "pret":"Pre&#355: 45<sup>94</sup> lei"
                    },
                    {
                      "img": "h.png",
                      "title":"Branz&#259 Proasp&#259t&#259 De Vac&#259 Napolact 175g",
                      "txt": "Din numa’ 3 p&#259hare de lapte bun din inima Ardealului facem cu drag pentru voi o br&acircnz&#259 proasp&#259t&#259, u&#351oar&#259 s&#259 v&#259 bucura&#355i de-o gustare cand v&#259 e pofta mai mare.",
                      "pret":"Pre&#355: 5<sup>66</sup> lei"
                    }
            ];
            window.onload = () => {
                  generateTemplate(products);
                  generateFooter();
            }

            function generateTemplate(obj) {
                const element = document.getElementById('products');
                let text = '';
                for(let i=0; i<obj.length; i++) {
            text += '<div class="col-md-3" onclick="showPopup(' + i + ')" data-toggle="modal" data-target="#myModal"><div class="imaginea"><img src="' + obj[i].img + '"></div></div>';
                }
                element.innerHTML = text;
            }

function showPopup(index) {
    const selected = products[index];
    console.log('info: ', selected);
    const popup = document.getElementById("popup");
    if(popup) {
        popup.innerHTML = '';
    popup.innerHTML = `
    <div id="myModal" class="modal fade" role="dialog" style="display: block; opacity: 1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" onclick="closePopup()">&times;</button>
            <h4 class="modal-title">${selected.title}</h4>
          </div>
          <div class="modal-body">
          <img src="${selected.img}"  style="margin: auto; display: flex; height: 50vh;">
            <p>${selected.txt}</p>
          </div>
          <div class="modal-pret">
            <p><blockquote>${selected.pret}</blockquote></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="closePopup()">&Icircnchide&#355i</button>
          </div>
        </div>
    
      </div>
    </div>`;
    }
}

function closePopup() {
    const popup = document.getElementById("popup");
    popup.innerHTML = '';
}
function generateFooter(elements){
  const footer = document.getElementById("footer");
  let text = '';
  text = `
<footer>
  <ul>`;

    for(let i= 0; i<elements.length; i++) {
      text += `<a href="${elements[i].url}"> <li>${elements[i].name}</li></a>`
    }
    text += `
</ul>
<div class="fb">
	   <a href="https://www.facebook.com/groups/2032567447026542/">
  <img src="fb-footer.png">
  </a> 
</div>
<div class="yt">
<a href="https://www.youtube.com/user/Linellamoldova">
<img src="yt-footer.png">
</a>
</div><footer>
  `;

  footer.innerHTML = text;
}